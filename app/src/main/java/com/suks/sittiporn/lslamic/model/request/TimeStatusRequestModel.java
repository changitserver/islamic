package com.suks.sittiporn.lslamic.model.request;

public class TimeStatusRequestModel {

    private String id;
    private int time_status1;
    private int time_status2;
    private int time_status3;
    private int time_status4;
    private int time_status5;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTime_status1() {
        return time_status1;
    }

    public void setTime_status1(int time_status1) {
        this.time_status1 = time_status1;
    }

    public int getTime_status2() {
        return time_status2;
    }

    public void setTime_status2(int time_status2) {
        this.time_status2 = time_status2;
    }

    public int getTime_status3() {
        return time_status3;
    }

    public void setTime_status3(int time_status3) {
        this.time_status3 = time_status3;
    }

    public int getTime_status4() {
        return time_status4;
    }

    public void setTime_status4(int time_status4) {
        this.time_status4 = time_status4;
    }

    public int getTime_status5() {
        return time_status5;
    }

    public void setTime_status5(int time_status5) {
        this.time_status5 = time_status5;
    }
}
