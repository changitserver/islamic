package com.suks.sittiporn.lslamic.model.reponse;

public class ExaminationListDataReponseModel {

    private String examination_list_id;
    private String examination_list_examination_id;
    private String examination_list_question;
    private String examination_list_a;
    private String examination_list_a_score;
    private String examination_list_b;
    private String examination_list_b_score;
    private String examination_list_c;
    private String examination_list_c_score;
    private String examination_list_d;
    private String examination_list_d_score;
    private String examination_list_e;
    private String examination_list_e_score;
    private int score;
    private boolean check;

    public String getExamination_list_id() {
        return examination_list_id;
    }

    public void setExamination_list_id(String examination_list_id) {
        this.examination_list_id = examination_list_id;
    }

    public String getExamination_list_examination_id() {
        return examination_list_examination_id;
    }

    public void setExamination_list_examination_id(String examination_list_examination_id) {
        this.examination_list_examination_id = examination_list_examination_id;
    }

    public String getExamination_list_question() {
        return examination_list_question;
    }

    public void setExamination_list_question(String examination_list_question) {
        this.examination_list_question = examination_list_question;
    }

    public String getExamination_list_a() {
        return examination_list_a;
    }

    public void setExamination_list_a(String examination_list_a) {
        this.examination_list_a = examination_list_a;
    }

    public String getExamination_list_a_score() {
        return examination_list_a_score;
    }

    public void setExamination_list_a_score(String examination_list_a_score) {
        this.examination_list_a_score = examination_list_a_score;
    }

    public String getExamination_list_b() {
        return examination_list_b;
    }

    public void setExamination_list_b(String examination_list_b) {
        this.examination_list_b = examination_list_b;
    }

    public String getExamination_list_b_score() {
        return examination_list_b_score;
    }

    public void setExamination_list_b_score(String examination_list_b_score) {
        this.examination_list_b_score = examination_list_b_score;
    }

    public String getExamination_list_c() {
        return examination_list_c;
    }

    public void setExamination_list_c(String examination_list_c) {
        this.examination_list_c = examination_list_c;
    }

    public String getExamination_list_c_score() {
        return examination_list_c_score;
    }

    public void setExamination_list_c_score(String examination_list_c_score) {
        this.examination_list_c_score = examination_list_c_score;
    }

    public String getExamination_list_d() {
        return examination_list_d;
    }

    public void setExamination_list_d(String examination_list_d) {
        this.examination_list_d = examination_list_d;
    }

    public String getExamination_list_d_score() {
        return examination_list_d_score;
    }

    public void setExamination_list_d_score(String examination_list_d_score) {
        this.examination_list_d_score = examination_list_d_score;
    }

    public String getExamination_list_e() {
        return examination_list_e;
    }

    public void setExamination_list_e(String examination_list_e) {
        this.examination_list_e = examination_list_e;
    }

    public String getExamination_list_e_score() {
        return examination_list_e_score;
    }

    public void setExamination_list_e_score(String examination_list_e_score) {
        this.examination_list_e_score = examination_list_e_score;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
