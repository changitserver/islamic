package com.suks.sittiporn.lslamic.model.reponse;

public class VipListDataResponseModel {

    private String vip_id;
    private String vip_status;
    private String vip_date_start;
    private String vip_date_end;

    public String getVip_id() {
        return vip_id;
    }

    public void setVip_id(String vip_id) {
        this.vip_id = vip_id;
    }

    public String getVip_status() {
        return vip_status;
    }

    public void setVip_status(String vip_status) {
        this.vip_status = vip_status;
    }

    public String getVip_date_start() {
        return vip_date_start;
    }

    public void setVip_date_start(String vip_date_start) {
        this.vip_date_start = vip_date_start;
    }

    public String getVip_date_end() {
        return vip_date_end;
    }

    public void setVip_date_end(String vip_date_end) {
        this.vip_date_end = vip_date_end;
    }
}
