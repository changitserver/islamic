package com.suks.sittiporn.lslamic.realm;

import io.realm.RealmObject;

public class LiginDataModelRealm extends RealmObject {

    private String id;
    private String email;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
