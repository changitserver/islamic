package com.suks.sittiporn.lslamic.model.request;

public class CreateStatusPaymentRequestModel {

    private String examination_status_head_id;
    private String examination_status_examination_id;
    private String examination_status_user_id;
    private String examination_status_user;
    private String examination_status_status;
    private String examination_status_slip;
    private String examination_status_group;

    public String getExamination_status_head_id() {
        return examination_status_head_id;
    }

    public void setExamination_status_head_id(String examination_status_head_id) {
        this.examination_status_head_id = examination_status_head_id;
    }

    public String getExamination_status_examination_id() {
        return examination_status_examination_id;
    }

    public void setExamination_status_examination_id(String examination_status_examination_id) {
        this.examination_status_examination_id = examination_status_examination_id;
    }

    public String getExamination_status_user_id() {
        return examination_status_user_id;
    }

    public void setExamination_status_user_id(String examination_status_user_id) {
        this.examination_status_user_id = examination_status_user_id;
    }

    public String getExamination_status_user() {
        return examination_status_user;
    }

    public void setExamination_status_user(String examination_status_user) {
        this.examination_status_user = examination_status_user;
    }

    public String getExamination_status_status() {
        return examination_status_status;
    }

    public void setExamination_status_status(String examination_status_status) {
        this.examination_status_status = examination_status_status;
    }

    public String getExamination_status_slip() {
        return examination_status_slip;
    }

    public void setExamination_status_slip(String examination_status_slip) {
        this.examination_status_slip = examination_status_slip;
    }

    public String getExamination_status_group() {
        return examination_status_group;
    }

    public void setExamination_status_group(String examination_status_group) {
        this.examination_status_group = examination_status_group;
    }
}
