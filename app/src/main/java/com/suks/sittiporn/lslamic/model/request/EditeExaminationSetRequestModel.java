package com.suks.sittiporn.lslamic.model.request;

public class EditeExaminationSetRequestModel {

    private String examination_set_id;
    private String examination_set_set;

    public String getExamination_set_id() {
        return examination_set_id;
    }

    public void setExamination_set_id(String examination_set_id) {
        this.examination_set_id = examination_set_id;
    }

    public String getExamination_set_set() {
        return examination_set_set;
    }

    public void setExamination_set_set(String examination_set_set) {
        this.examination_set_set = examination_set_set;
    }
}
