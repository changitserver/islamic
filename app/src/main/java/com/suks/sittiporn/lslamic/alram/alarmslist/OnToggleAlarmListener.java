package com.suks.sittiporn.lslamic.alram.alarmslist;

import com.suks.sittiporn.lslamic.alram.data.Alarm;

public interface OnToggleAlarmListener {
    void onToggle(Alarm alarm);
}
