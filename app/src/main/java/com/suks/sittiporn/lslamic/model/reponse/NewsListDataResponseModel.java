package com.suks.sittiporn.lslamic.model.reponse;

public class NewsListDataResponseModel {

    private String rongphai_news_id;
    private String rongphai_news_head;
    private String rongphai_news_descrition;
    private String rongphai_news_img;
    private String rongphai_news_date;
    private String rongphai_news_time;

    public String getRongphai_news_id() {
        return rongphai_news_id;
    }

    public void setRongphai_news_id(String rongphai_news_id) {
        this.rongphai_news_id = rongphai_news_id;
    }

    public String getRongphai_news_head() {
        return rongphai_news_head;
    }

    public void setRongphai_news_head(String rongphai_news_head) {
        this.rongphai_news_head = rongphai_news_head;
    }

    public String getRongphai_news_descrition() {
        return rongphai_news_descrition;
    }

    public void setRongphai_news_descrition(String rongphai_news_descrition) {
        this.rongphai_news_descrition = rongphai_news_descrition;
    }

    public String getRongphai_news_img() {
        return rongphai_news_img;
    }

    public void setRongphai_news_img(String rongphai_news_img) {
        this.rongphai_news_img = rongphai_news_img;
    }

    public String getRongphai_news_date() {
        return rongphai_news_date;
    }

    public void setRongphai_news_date(String rongphai_news_date) {
        this.rongphai_news_date = rongphai_news_date;
    }

    public String getRongphai_news_time() {
        return rongphai_news_time;
    }

    public void setRongphai_news_time(String rongphai_news_time) {
        this.rongphai_news_time = rongphai_news_time;
    }
}
