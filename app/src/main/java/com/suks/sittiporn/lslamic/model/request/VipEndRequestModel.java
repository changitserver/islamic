package com.suks.sittiporn.lslamic.model.request;

public class VipEndRequestModel {

    private String vip_id;
    private String vip_status;

    public String getVip_id() {
        return vip_id;
    }

    public void setVip_id(String vip_id) {
        this.vip_id = vip_id;
    }

    public String getVip_status() {
        return vip_status;
    }

    public void setVip_status(String vip_status) {
        this.vip_status = vip_status;
    }
}
