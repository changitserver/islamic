package com.suks.sittiporn.lslamic.model.reponse;

public class ShipListDataReponseModel {

    private int rowNumber;
    private boolean canApprove;
    private String approveStatus;
    private String approveStatusName;
    private String permitExpireStatus;
    private String permitExpireStatusName;
    private String cer285Status;
    private String cer285StatusName;
    private String hCert285Key;
    private String atyabatExpireStatus;
    private String atyabatExpireStatusName;
    private String shipRegisterNo;
    private String shipRegisterNoKey;
    private String shipName;
    private String shipOwnerName;
    private String tonGross;
    private String fishingAreaName;
    private String durationFishing;
    private String fishingGearKind;
    private String cer285Number;
    private String registerProvinceName;
    private String shipStatusName;
    private String shipStatus;
    private String hasLocked;
    private String shipPicture;
    private String permitExpireDate;

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public boolean isCanApprove() {
        return canApprove;
    }

    public void setCanApprove(boolean canApprove) {
        this.canApprove = canApprove;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getApproveStatusName() {
        return approveStatusName;
    }

    public void setApproveStatusName(String approveStatusName) {
        this.approveStatusName = approveStatusName;
    }

    public String getPermitExpireStatus() {
        return permitExpireStatus;
    }

    public void setPermitExpireStatus(String permitExpireStatus) {
        this.permitExpireStatus = permitExpireStatus;
    }

    public String getPermitExpireStatusName() {
        return permitExpireStatusName;
    }

    public void setPermitExpireStatusName(String permitExpireStatusName) {
        this.permitExpireStatusName = permitExpireStatusName;
    }

    public String getCer285Status() {
        return cer285Status;
    }

    public void setCer285Status(String cer285Status) {
        this.cer285Status = cer285Status;
    }

    public String getCer285StatusName() {
        return cer285StatusName;
    }

    public void setCer285StatusName(String cer285StatusName) {
        this.cer285StatusName = cer285StatusName;
    }

    public String gethCert285Key() {
        return hCert285Key;
    }

    public void sethCert285Key(String hCert285Key) {
        this.hCert285Key = hCert285Key;
    }

    public String getAtyabatExpireStatus() {
        return atyabatExpireStatus;
    }

    public void setAtyabatExpireStatus(String atyabatExpireStatus) {
        this.atyabatExpireStatus = atyabatExpireStatus;
    }

    public String getAtyabatExpireStatusName() {
        return atyabatExpireStatusName;
    }

    public void setAtyabatExpireStatusName(String atyabatExpireStatusName) {
        this.atyabatExpireStatusName = atyabatExpireStatusName;
    }

    public String getShipRegisterNo() {
        return shipRegisterNo;
    }

    public void setShipRegisterNo(String shipRegisterNo) {
        this.shipRegisterNo = shipRegisterNo;
    }

    public String getShipRegisterNoKey() {
        return shipRegisterNoKey;
    }

    public void setShipRegisterNoKey(String shipRegisterNoKey) {
        this.shipRegisterNoKey = shipRegisterNoKey;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipOwnerName() {
        return shipOwnerName;
    }

    public void setShipOwnerName(String shipOwnerName) {
        this.shipOwnerName = shipOwnerName;
    }

    public String getTonGross() {
        return tonGross;
    }

    public void setTonGross(String tonGross) {
        this.tonGross = tonGross;
    }

    public String getFishingAreaName() {
        return fishingAreaName;
    }

    public void setFishingAreaName(String fishingAreaName) {
        this.fishingAreaName = fishingAreaName;
    }

    public String getDurationFishing() {
        return durationFishing;
    }

    public void setDurationFishing(String durationFishing) {
        this.durationFishing = durationFishing;
    }

    public String getFishingGearKind() {
        return fishingGearKind;
    }

    public void setFishingGearKind(String fishingGearKind) {
        this.fishingGearKind = fishingGearKind;
    }

    public String getCer285Number() {
        return cer285Number;
    }

    public void setCer285Number(String cer285Number) {
        this.cer285Number = cer285Number;
    }

    public String getRegisterProvinceName() {
        return registerProvinceName;
    }

    public void setRegisterProvinceName(String registerProvinceName) {
        this.registerProvinceName = registerProvinceName;
    }

    public String getShipStatusName() {
        return shipStatusName;
    }

    public void setShipStatusName(String shipStatusName) {
        this.shipStatusName = shipStatusName;
    }

    public String getShipStatus() {
        return shipStatus;
    }

    public void setShipStatus(String shipStatus) {
        this.shipStatus = shipStatus;
    }

    public String getHasLocked() {
        return hasLocked;
    }

    public void setHasLocked(String hasLocked) {
        this.hasLocked = hasLocked;
    }

    public String getShipPicture() {
        return shipPicture;
    }

    public void setShipPicture(String shipPicture) {
        this.shipPicture = shipPicture;
    }

    public String getPermitExpireDate() {
        return permitExpireDate;
    }

    public void setPermitExpireDate(String permitExpireDate) {
        this.permitExpireDate = permitExpireDate;
    }
}
