package com.suks.sittiporn.lslamic.model.reponse;

public class GetStatusDataReponseModel {

    private String examination_status_status;
    private String examination_status_id;

    public String getExamination_status_status() {
        return examination_status_status;
    }

    public void setExamination_status_status(String examination_status_status) {
        this.examination_status_status = examination_status_status;
    }

    public String getExamination_status_id() {
        return examination_status_id;
    }

    public void setExamination_status_id(String examination_status_id) {
        this.examination_status_id = examination_status_id;
    }
}
