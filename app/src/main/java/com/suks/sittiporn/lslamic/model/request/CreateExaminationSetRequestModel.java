package com.suks.sittiporn.lslamic.model.request;

public class CreateExaminationSetRequestModel {

    private String examination_set_id_connect;
    private String examination_set_set;
    private String examination_set_type;
    private String examination_set_create_date;

    public String getExamination_set_id_connect() {
        return examination_set_id_connect;
    }

    public void setExamination_set_id_connect(String examination_set_id_connect) {
        this.examination_set_id_connect = examination_set_id_connect;
    }

    public String getExamination_set_set() {
        return examination_set_set;
    }

    public void setExamination_set_set(String examination_set_set) {
        this.examination_set_set = examination_set_set;
    }

    public String getExamination_set_type() {
        return examination_set_type;
    }

    public void setExamination_set_type(String examination_set_type) {
        this.examination_set_type = examination_set_type;
    }

    public String getExamination_set_create_date() {
        return examination_set_create_date;
    }

    public void setExamination_set_create_date(String examination_set_create_date) {
        this.examination_set_create_date = examination_set_create_date;
    }
}
