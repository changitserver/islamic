package com.suks.sittiporn.lslamic.model.request;

public class EditeExaminationHeadRequestModel {

    private String examination_head_id;
    private String examination_head_name;

    public String getExamination_head_id() {
        return examination_head_id;
    }

    public void setExamination_head_id(String examination_head_id) {
        this.examination_head_id = examination_head_id;
    }

    public String getExamination_head_name() {
        return examination_head_name;
    }

    public void setExamination_head_name(String examination_head_name) {
        this.examination_head_name = examination_head_name;
    }
}
