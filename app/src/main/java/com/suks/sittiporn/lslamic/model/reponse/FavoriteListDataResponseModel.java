package com.suks.sittiporn.lslamic.model.reponse;

public class FavoriteListDataResponseModel {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
