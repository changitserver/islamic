package com.suks.sittiporn.lslamic.model.reponse;

public class AddCommentReponseModel {

    private String location_id;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }
}
