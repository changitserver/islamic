package com.suks.sittiporn.lslamic.model.reponse;

public class ImgReponseModel {

    private String image_url;

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
