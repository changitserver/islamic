package com.suks.sittiporn.lslamic.model.reponse;

public class TimeReponseModel {

    private String time_name1;
    private String time_name2;
    private String time_name3;
    private String time_name4;
    private String time_name5;

    private String time_id;
    private String time_status1;
    private String time_status2;
    private String time_status3;
    private String time_status4;
    private String time_status5;

    public String getTime_name1() {
        return time_name1;
    }

    public void setTime_name1(String time_name1) {
        this.time_name1 = time_name1;
    }

    public String getTime_name2() {
        return time_name2;
    }

    public void setTime_name2(String time_name2) {
        this.time_name2 = time_name2;
    }

    public String getTime_id() {
        return time_id;
    }

    public void setTime_id(String time_id) {
        this.time_id = time_id;
    }

    public String getTime_status1() {
        return time_status1;
    }

    public void setTime_status1(String time_status1) {
        this.time_status1 = time_status1;
    }

    public String getTime_status2() {
        return time_status2;
    }

    public void setTime_status2(String time_status2) {
        this.time_status2 = time_status2;
    }

    public String getTime_status3() {
        return time_status3;
    }

    public void setTime_status3(String time_status3) {
        this.time_status3 = time_status3;
    }

    public String getTime_status4() {
        return time_status4;
    }

    public void setTime_status4(String time_status4) {
        this.time_status4 = time_status4;
    }

    public String getTime_status5() {
        return time_status5;
    }

    public void setTime_status5(String time_status5) {
        this.time_status5 = time_status5;
    }

    public String getTime_name3() {
        return time_name3;
    }

    public void setTime_name3(String time_name3) {
        this.time_name3 = time_name3;
    }

    public String getTime_name4() {
        return time_name4;
    }

    public void setTime_name4(String time_name4) {
        this.time_name4 = time_name4;
    }

    public String getTime_name5() {
        return time_name5;
    }

    public void setTime_name5(String time_name5) {
        this.time_name5 = time_name5;
    }


}
