package com.suks.sittiporn.lslamic.model.reponse;

public class HistoryListDataReponseModel {

    private String history_id;
    private String history_score;
    private String history_total_exam;
    private String history_date;
    private String history_time;
    private String history_set_name;
    private String history_head_name;
    private String history_head_id_connection;
    private String history_type_name;

    public String getHistory_id() {
        return history_id;
    }

    public void setHistory_id(String history_id) {
        this.history_id = history_id;
    }

    public String getHistory_score() {
        return history_score;
    }

    public void setHistory_score(String history_score) {
        this.history_score = history_score;
    }

    public String getHistory_total_exam() {
        return history_total_exam;
    }

    public void setHistory_total_exam(String history_total_exam) {
        this.history_total_exam = history_total_exam;
    }

    public String getHistory_date() {
        return history_date;
    }

    public void setHistory_date(String history_date) {
        this.history_date = history_date;
    }

    public String getHistory_time() {
        return history_time;
    }

    public void setHistory_time(String history_time) {
        this.history_time = history_time;
    }

    public String getHistory_set_name() {
        return history_set_name;
    }

    public void setHistory_set_name(String history_set_name) {
        this.history_set_name = history_set_name;
    }

    public String getHistory_head_name() {
        return history_head_name;
    }

    public void setHistory_head_name(String history_head_name) {
        this.history_head_name = history_head_name;
    }

    public String getHistory_head_id_connection() {
        return history_head_id_connection;
    }

    public void setHistory_head_id_connection(String history_head_id_connection) {
        this.history_head_id_connection = history_head_id_connection;
    }

    public String getHistory_type_name() {
        return history_type_name;
    }

    public void setHistory_type_name(String history_type_name) {
        this.history_type_name = history_type_name;
    }
}
